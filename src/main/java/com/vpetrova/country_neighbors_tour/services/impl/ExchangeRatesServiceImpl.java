package com.vpetrova.country_neighbors_tour.services.impl;

import com.vpetrova.country_neighbors_tour.dto.ExchangeRateDto;
import com.vpetrova.country_neighbors_tour.entities.ExchangeRatesEntity;
import com.vpetrova.country_neighbors_tour.properties.ExchangeRatesProperty;
import com.vpetrova.country_neighbors_tour.repositories.ExchangeRateRepository;
import com.vpetrova.country_neighbors_tour.services.ExchangeRatesService;
import javax.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional
public class ExchangeRatesServiceImpl implements ExchangeRatesService {

  private final ExchangeRatesProperty exchangeRatesProperty;
  private final ExchangeRateRepository exchangeRateRepository;
  private final RestTemplate restTemplate;
  private final ModelMapper mapper;

  public ExchangeRatesServiceImpl(ExchangeRatesProperty exchangeRatesProperty,
      ExchangeRateRepository exchangeRateRepository, RestTemplate restTemplate, ModelMapper mapper) {
    this.exchangeRatesProperty = exchangeRatesProperty;
    this.exchangeRateRepository = exchangeRateRepository;
    this.restTemplate = restTemplate;

    this.mapper = mapper;
  }

  @Override
  public ExchangeRateDto findExchanged() {
    return mapper.map(exchangeRateRepository.findExchanged(), ExchangeRateDto.class);

  }

  @Override
  public ExchangeRateDto save(ExchangeRateDto exchangeRateDto) {
    ExchangeRatesEntity exchangeRatesEntity = exchangeRateRepository.save(mapper.map(exchangeRateDto,
        ExchangeRatesEntity.class));
    return mapper.map(exchangeRatesEntity, ExchangeRateDto.class);
  }

  @Override
  public ExchangeRateDto getCurrency() {
    ResponseEntity<ExchangeRateDto> exchangedRate = restTemplate.getForEntity(exchangeRatesProperty.getUrl(),
        ExchangeRateDto.class);
    return exchangedRate.getBody();
  }
}
