package com.vpetrova.country_neighbors_tour.controllers.impl;

import com.vpetrova.country_neighbors_tour.controllers.ExchangedRatesController;
import com.vpetrova.country_neighbors_tour.controllers.TripData;
import com.vpetrova.country_neighbors_tour.dto.TripBudgetDto;
import com.vpetrova.country_neighbors_tour.dto.TripDataDto;
import com.vpetrova.country_neighbors_tour.services.ExchangeRatesService;
import com.vpetrova.country_neighbors_tour.services.NeighborCountriesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExchangedRatesControllerImpl implements ExchangedRatesController {

  private final NeighborCountriesService neighborCountriesService;
  private final ExchangeRatesService exchangeRatesService;

  public ExchangedRatesControllerImpl(NeighborCountriesService neighborCountriesService, ExchangeRatesService exchangeRatesService) {
    this.neighborCountriesService = neighborCountriesService;
    this.exchangeRatesService = exchangeRatesService;
  }

  @Override
  public ResponseEntity<TripDataDto> getTripData(TripBudgetDto tripBudgetDto) {
    return ResponseEntity.ok().body(TripData.getTripDataDto(
        tripBudgetDto,
        neighborCountriesService.findAllNeighborsByCountry(tripBudgetDto.getHomeCountry()),
        exchangeRatesService.findExchanged()));
  }
}
