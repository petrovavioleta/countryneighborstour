package com.vpetrova.country_neighbors_tour.repositories;

import com.vpetrova.country_neighbors_tour.entities.ExchangeRatesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRatesEntity, Long> {

  @Query(value = ""
      + "SELECT pk_latest_rate, base, date, success, \"timestamp\" "
      + "FROM country_trip.public.latest_rates "
      + "ORDER BY pk_latest_rate "
      + "DESC LIMIT 1", nativeQuery = true)
  ExchangeRatesEntity findExchanged();

}
