package com.vpetrova.country_neighbors_tour.controllers;

import com.vpetrova.country_neighbors_tour.dto.ExchangeRateDto;
import com.vpetrova.country_neighbors_tour.dto.NeighborCountryDto;
import com.vpetrova.country_neighbors_tour.dto.TripBudgetDto;
import com.vpetrova.country_neighbors_tour.dto.TripDataDto;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TripData {

  public static TripDataDto getTripDataDto(
      TripBudgetDto tripBudgetDto,
      List<NeighborCountryDto> neighborCountryDtoList,
      ExchangeRateDto exchangeRateDto) {

    Double allCountries = tripBudgetDto.getBudgetPerCountry() * neighborCountryDtoList.size();
    Integer tripTimes = (int) ((tripBudgetDto.getTotalBudget() / (allCountries)));

    return TripDataDto.builder()
        .leftover(tripBudgetDto.getTotalBudget() - (allCountries * tripTimes))
        .perDestinationVisits(tripTimes)
        .neighbourCountryCurrencyRate(getNeighborsCountryCurrencyRate(tripBudgetDto, neighborCountryDtoList, exchangeRateDto))
        .build();
  }

  private static Map<String, BigDecimal> getNeighborsCountryCurrencyRate(
      TripBudgetDto tripBudgetDto, List<NeighborCountryDto> neighborCountryDtoList,
      ExchangeRateDto exchangeRateDto) {

    Map<String, BigDecimal> neighbourCountryCurrencyRate = new HashMap<>();
    neighborCountryDtoList.forEach(country -> {

      if (country.getCountyCurrencyIsoCode() == null) {
        neighbourCountryCurrencyRate.put(country.getCountryBorderName(),
            BigDecimal.valueOf(tripBudgetDto.getBudgetPerCountry()));
      } else {

        neighbourCountryCurrencyRate.put(country.getCountryBorderName(),
            exchangeRateDto.getRates().get(country.getCountyCurrencyIsoCode()).multiply(BigDecimal
                .valueOf(tripBudgetDto.getBudgetPerCountry())));
      }
    });

    return neighbourCountryCurrencyRate;
  }

}
