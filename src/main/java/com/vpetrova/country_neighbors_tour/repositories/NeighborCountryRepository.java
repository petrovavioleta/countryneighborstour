package com.vpetrova.country_neighbors_tour.repositories;

import com.vpetrova.country_neighbors_tour.entities.NeighborCountriesEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NeighborCountryRepository extends JpaRepository<NeighborCountriesEntity, Long> {

  List<NeighborCountriesEntity> findAllByCountryName(String homeCountry);

}
