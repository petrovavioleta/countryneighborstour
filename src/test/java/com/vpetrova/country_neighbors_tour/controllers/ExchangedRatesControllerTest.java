package com.vpetrova.country_neighbors_tour.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vpetrova.country_neighbors_tour.controllers.impl.ExchangedRatesControllerImpl;
import com.vpetrova.country_neighbors_tour.dto.ExchangeRateDto;
import com.vpetrova.country_neighbors_tour.dto.NeighborCountryDto;
import com.vpetrova.country_neighbors_tour.dto.TripBudgetDto;
import com.vpetrova.country_neighbors_tour.services.ExchangeRatesService;
import com.vpetrova.country_neighbors_tour.services.NeighborCountriesService;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = {ExchangedRatesController.class})
@AutoConfigureMockMvc(addFilters = false)
public class ExchangedRatesControllerTest {

  private static final String BASE_CONTROLLER_PATH = "/api/trip/angel";

  @Autowired
  protected MockMvc mockMvc;
  @MockBean
  private NeighborCountriesService neighborCountriesService;
  @MockBean
  private ExchangeRatesService exchangeRatesService;
  @Mock
  private TripData tripData;
  @MockBean
  private ExchangeRateDto exchangeRateDto;
  @InjectMocks
  private ExchangedRatesControllerImpl exchangedRatesController;

  @Test
  void getTripData() throws Exception {

    Double budgetPerCountry = 212.5;
    Double totalBudget = 36000.99;
    String country = "Bangladesh";
    String base = "base";

    TripBudgetDto tripBudgetDto = TripBudgetDto.builder()
        .budgetPerCountry(budgetPerCountry)
        .totalBudget(totalBudget)
        .homeCountry(country)
        .build();

    Map<String, BigDecimal> map = Map.of("MMK", new BigDecimal(34));

    ExchangeRateDto exchangeRateDto = ExchangeRateDto.builder()
        .date(new Date())
        .success(true)
        .timestamp(12348543516L)
        .rates(map)
        .base(base)
        .build();

    List<NeighborCountryDto> neighborCountryDtoList = Collections.singletonList(NeighborCountryDto.builder()
        .countryBorderCode("countryBorderCode")
        .countryBorderName("countryBorderName")
        .countryCode("countryCode")
        .countyCurrencyIsoCode("MMK")
        .build());

    when(neighborCountriesService.findAllNeighborsByCountry(country)).thenReturn(neighborCountryDtoList);
    when(exchangeRatesService.findExchanged()).thenReturn(exchangeRateDto);

    ObjectMapper objectMapper = new ObjectMapper();
    String json = objectMapper.writeValueAsString(tripBudgetDto);

    mockMvc.perform(
        post(BASE_CONTROLLER_PATH)
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
        .andDo(print())
        .andExpect(jsonPath("$").exists())
        .andExpect(status().isOk());
  }

}
