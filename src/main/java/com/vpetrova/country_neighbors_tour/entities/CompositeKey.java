package com.vpetrova.country_neighbors_tour.entities;

import java.io.Serializable;

public class CompositeKey implements Serializable {

  private String countryCode;
  private String countryBorderCode;

}
