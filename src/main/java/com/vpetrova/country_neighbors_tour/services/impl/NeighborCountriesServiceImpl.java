package com.vpetrova.country_neighbors_tour.services.impl;

import com.vpetrova.country_neighbors_tour.dto.NeighborCountryDto;
import com.vpetrova.country_neighbors_tour.repositories.NeighborCountryRepository;
import com.vpetrova.country_neighbors_tour.services.NeighborCountriesService;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class NeighborCountriesServiceImpl implements NeighborCountriesService {

  private final NeighborCountryRepository neighborCountryRepository;
  private final ModelMapper modelMapper;

  public NeighborCountriesServiceImpl(NeighborCountryRepository neighborCountryRepository, ModelMapper modelMapper) {
    this.neighborCountryRepository = neighborCountryRepository;
    this.modelMapper = modelMapper;
  }

  @Override
  public List<NeighborCountryDto> findAllNeighborsByCountry(String country) {
    return neighborCountryRepository.findAllByCountryName(country).stream()
        .map(entity -> modelMapper.map(entity, NeighborCountryDto.class))
        .collect(Collectors.toList());
  }
}
