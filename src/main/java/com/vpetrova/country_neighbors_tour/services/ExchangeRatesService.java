package com.vpetrova.country_neighbors_tour.services;

import com.vpetrova.country_neighbors_tour.dto.ExchangeRateDto;

public interface ExchangeRatesService {

  ExchangeRateDto findExchanged();

  ExchangeRateDto save(ExchangeRateDto exchangeRateDto);

  ExchangeRateDto getCurrency();
}
