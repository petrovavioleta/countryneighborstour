package com.vpetrova.country_neighbors_tour.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.vpetrova.country_neighbors_tour.dto.NeighborCountryDto;
import com.vpetrova.country_neighbors_tour.entities.NeighborCountriesEntity;
import com.vpetrova.country_neighbors_tour.repositories.NeighborCountryRepository;
import com.vpetrova.country_neighbors_tour.services.impl.NeighborCountriesServiceImpl;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.web.client.RestTemplate;

@ExtendWith({MockitoExtension.class})
public class NeighborCountryServiceTest {

  @Mock
  private NeighborCountryRepository neighborCountryRepository;
  @Mock
  private RestTemplate restTemplate;
  @Spy
  private ModelMapper modelMapper;
  @InjectMocks
  private NeighborCountriesServiceImpl neighborCountriesService;

  @Test
  void findAllNeighborsByCountry_andExpect_noErrors() {

    String country = "Bangladesh";

    List<NeighborCountriesEntity> response = Collections.singletonList(NeighborCountriesEntity.builder()
        .countryBorderCode("countryBorderCode")
        .countryBorderName("countryBorderName")
        .countryCode("countryCode")
        .countyCurrencyIsoCode("MMK")
        .build());

    when(neighborCountryRepository.findAllByCountryName(any())).thenReturn(response);

    List<NeighborCountryDto> result = neighborCountriesService.findAllNeighborsByCountry(country);

    assertEquals(1, result.size());
    assertEquals(response.get(0).getCountryBorderName(), result.get(0).getCountryBorderName());
    assertEquals(response.get(0).getCountryName(), result.get(0).getCountryName());
    assertEquals(response.get(0).getCountryCode(), result.get(0).getCountryCode());
    assertEquals(response.get(0).getCountryBorderCode(), result.get(0).getCountryBorderCode());
  }

}
