package com.vpetrova.country_neighbors_tour.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "exchange_rates")
@Table(name = "latest_rates")
public class ExchangeRatesEntity {

  @Id
  @Column(name = "pk_latest_rate")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "success")
  private boolean success;

  @Column(name = "timestamp")
  private Long timestamp;

  @Column(name = "base")
  private String base;

  @Column(name = "date")
  private Date date;

  @ElementCollection(fetch = FetchType.LAZY)
  @CollectionTable(name = "latest_rates_map_of_rates", joinColumns = @JoinColumn(name = "pk_rates_id",
      referencedColumnName = "pk_latest_rate"))
  @MapKeyColumn(name = "currency_iso_code", length = 5)
  @Column(name = "currency_value")
  private Map<String, BigDecimal> rates;
}
