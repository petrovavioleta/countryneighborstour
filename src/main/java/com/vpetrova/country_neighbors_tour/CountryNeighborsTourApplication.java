package com.vpetrova.country_neighbors_tour;

import com.vpetrova.country_neighbors_tour.properties.ExchangeRatesProperty;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ExchangeRatesProperty.class})
public class CountryNeighborsTourApplication {

  public static void main(String[] args) {
    SpringApplication.run(CountryNeighborsTourApplication.class, args);
  }

}
