package com.vpetrova.country_neighbors_tour.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRateDto {

  private boolean success;
  private Long timestamp;
  private String base;
  private Date date;
  private Map<String, BigDecimal> rates;

}
