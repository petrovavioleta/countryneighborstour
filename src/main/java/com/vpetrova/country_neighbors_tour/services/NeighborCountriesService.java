package com.vpetrova.country_neighbors_tour.services;

import com.vpetrova.country_neighbors_tour.dto.NeighborCountryDto;
import java.util.List;

public interface NeighborCountriesService {

  List<NeighborCountryDto> findAllNeighborsByCountry(String country);

}
