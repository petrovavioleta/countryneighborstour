package com.vpetrova.country_neighbors_tour.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "currency.exchange.rate")
public class ExchangeRatesProperty {

  private String url;

}
