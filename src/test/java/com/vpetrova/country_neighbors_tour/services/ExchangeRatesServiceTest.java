package com.vpetrova.country_neighbors_tour.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.vpetrova.country_neighbors_tour.dto.ExchangeRateDto;
import com.vpetrova.country_neighbors_tour.entities.ExchangeRatesEntity;
import com.vpetrova.country_neighbors_tour.properties.ExchangeRatesProperty;
import com.vpetrova.country_neighbors_tour.repositories.ExchangeRateRepository;
import com.vpetrova.country_neighbors_tour.services.impl.ExchangeRatesServiceImpl;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class ExchangeRatesServiceTest {

  @Spy
  private ModelMapper modelMapper;
  @Mock
  private ExchangeRateRepository exchangeRateRepository;
  @Mock
  private RestTemplate restTemplate;
  @Mock
  private ExchangeRatesProperty exchangeRatesProperty;
  @InjectMocks
  private ExchangeRatesServiceImpl exchangeRatesService;

  @Test
  void findLatest_andExpect_noErrors() {

    Double budgetPerCountry = 1500.1;
    Double totalBudget = 41000.2;
    String country = "Bangladesh";
    String base = "base";
    Map<String, BigDecimal> map = Map.of("key", new BigDecimal(27));

    ExchangeRatesEntity response = ExchangeRatesEntity.builder()
        .date(new Date())
        .success(true)
        .timestamp(6564846516L)
        .rates(map)
        .base(base)
        .build();

    when(exchangeRateRepository.findExchanged()).thenReturn(response);

    ExchangeRateDto result = exchangeRatesService.findExchanged();

    assertEquals(response.getBase(), result.getBase());
    assertEquals(response.getDate(), result.getDate());
    assertEquals(response.isSuccess(), result.isSuccess());
    assertEquals(response.getTimestamp(), result.getTimestamp());
    assertEquals(response.getRates(), result.getRates());
  }

  @Test
  void getCurrencyExchangeRate_andExpect_noErrors() {

    String url = "http://data.fixer.io/api/latest?access_key=a32b59bb7c3241271294777b619f61ee&format=1";
    Map<String, BigDecimal> map = Map.of("key", new BigDecimal(12));

    ExchangeRateDto response = ExchangeRateDto.builder()
        .date(new Date())
        .success(true)
        .timestamp(1264812516L)
        .rates(map)
        .base(url)
        .build();

    when(exchangeRatesProperty.getUrl()).thenReturn(url);
    when(restTemplate.getForEntity(url, ExchangeRateDto.class)).thenReturn(
        new ResponseEntity(response, HttpStatus.OK));
    ExchangeRateDto result = exchangeRatesService.getCurrency();

    assertEquals(response.getBase(), result.getBase());
    assertEquals(response.getDate(), result.getDate());
    assertEquals(response.getTimestamp(), result.getTimestamp());
    assertEquals(response.getRates(), result.getRates());
    assertEquals(response.isSuccess(), result.isSuccess());
  }

  @Test
  void saveRates_andExpect_noErrors() {

    String base = "base";

    Map<String, BigDecimal> map = Map.of("key", new BigDecimal(12));

    ExchangeRateDto request = ExchangeRateDto.builder()
        .date(new Date())
        .success(true)
        .timestamp(1264812516L)
        .rates(map)
        .base(base)
        .build();

    ExchangeRatesEntity response = ExchangeRatesEntity.builder()
        .date(new Date())
        .success(true)
        .timestamp(1264126516L)
        .rates(map)
        .base(base)
        .build();

    when(exchangeRateRepository.save(any())).thenReturn(response);

    ExchangeRateDto result = exchangeRatesService.save(request);

    assertEquals(response.getRates(), result.getRates());
    assertEquals(response.getTimestamp(), result.getTimestamp());
    assertEquals(response.getDate(), result.getDate());
    assertEquals(response.getBase(), result.getBase());
    assertEquals(response.isSuccess(), result.isSuccess());
  }

}
