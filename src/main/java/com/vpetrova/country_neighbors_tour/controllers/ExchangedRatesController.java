package com.vpetrova.country_neighbors_tour.controllers;

import com.vpetrova.country_neighbors_tour.dto.TripBudgetDto;
import com.vpetrova.country_neighbors_tour.dto.TripDataDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/trip/angel")
public interface ExchangedRatesController {

  @PostMapping(consumes = "application/json")
  ResponseEntity<TripDataDto> getTripData(@RequestBody TripBudgetDto tripBudgetDto);
}
