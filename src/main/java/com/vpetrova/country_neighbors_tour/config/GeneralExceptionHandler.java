package com.vpetrova.country_neighbors_tour.config;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RestControllerAdvice
@EnableWebMvc
public class GeneralExceptionHandler {

  public static Map<String, Object> buildErrorResponse(HttpStatus status, String localizedMessage) {
    Map<String, Object> response = new HashMap<>();
    response.put("status", status.name());
    response.put("message", localizedMessage);
    return response;
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public Map<String, Object> handleException(Exception e) {
    e.printStackTrace();
    return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
  }

  @ExceptionHandler({NoHandlerFoundException.class, NoSuchElementException.class})
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public Map<String, Object> handleNoHandlerFound(NoHandlerFoundException e) {
    e.printStackTrace();
    return buildErrorResponse(HttpStatus.NOT_FOUND, e.getLocalizedMessage());
  }
}