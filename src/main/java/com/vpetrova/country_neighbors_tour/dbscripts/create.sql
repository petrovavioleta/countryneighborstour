create table public.country_borders
(
    country_border_code varchar(255) not null,
    country_code          varchar(255) not null,
    country_border_name varchar(255),
    country_name          varchar(255),
    primary key (country_border_code, country_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

create table public.latest_rates
(
    pk_latest_rate bigint not null auto_increment,
    base             varchar(255),
    date             datetime,
    success          bit,
    timestamp        bigint,
    primary key (pk_latest_rate)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

create table public.latest_rates_map_of_rates
(
    pk_rates_id       int8       not null,
    currency_value    numeric(19, 2),
    currency_iso_code varchar(5) not null,
    primary key (pk_rates_id, currency_iso_code)
);
