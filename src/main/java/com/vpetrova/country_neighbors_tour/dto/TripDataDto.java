package com.vpetrova.country_neighbors_tour.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TripDataDto {

  private Integer perDestinationVisits;
  private Double leftover;
  private Map<String, BigDecimal> neighbourCountryCurrencyRate;

}
