package com.vpetrova.country_neighbors_tour.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TripBudgetDto {

  private String homeCountry;
  private Double budgetPerCountry;
  private Double totalBudget;

}
